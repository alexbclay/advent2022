use std::collections::HashSet;

use crate::Solver;

pub struct DayEightSolver {
    map: Vec<Vec<u8>>,
}

impl DayEightSolver {
    fn get_view_score(&self, col_idx: usize, row_idx: usize) -> usize {
        let max_col = self.map.len();
        let max_row = self.map[0].len();

        if col_idx == 0 || col_idx == max_col - 1 || row_idx == 0 || row_idx == max_row - 1 {
            return 0;
        }

        let height = self.map[col_idx][row_idx];
        // multiplicative identity
        let mut score = 1;

        for (distance, idx) in (col_idx as usize + 1..max_col).enumerate() {
            let val = self.map[idx][row_idx];
            if val >= height {
                score *= distance + 1;
                break;
            }
            if idx == max_col - 1 {
                // got to the end without finding a block, take the distance
                score *= distance + 1;
                break;
            }
        }

        for (distance, idx) in (0..col_idx as usize).rev().enumerate() {
            let val = self.map[idx][row_idx];
            if val >= height {
                score *= distance + 1;
                break;
            }

            if idx == 0 {
                // got to the end without finding a block, take the distance                // got to the end, take that
                score *= distance + 1;
                break;
            }
        }

        for (distance, idx) in (row_idx as usize + 1..max_row).enumerate() {
            let val = self.map[col_idx][idx];
            if val >= height {
                score *= distance + 1;
                break;
            }
            if idx == max_row - 1 {
                // got to the end, take that
                score *= distance + 1;
                break;
            }
        }

        for (distance, idx) in (0..row_idx as usize).rev().enumerate() {
            let val = self.map[col_idx][idx];
            if val >= height {
                score *= distance + 1;
                break;
            }
            if idx == 0 {
                // got to the end, take that
                score *= distance + 1;
                break;
            }
        }

        score
    }
}

impl Solver for DayEightSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DayEightSolver {
            map: input
                .lines()
                .map(|line| {
                    line.chars()
                        .map(|character| match character {
                            '0' => 0,
                            '1' => 1,
                            '2' => 2,
                            '3' => 3,
                            '4' => 4,
                            '5' => 5,
                            '6' => 6,
                            '7' => 7,
                            '8' => 8,
                            '9' => 9,

                            _ => panic!("Unexpected char in input map!"),
                        })
                        .collect()
                })
                .collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut visible: HashSet<(usize, usize)> = HashSet::new();

        // visible from left, visible from right
        for (col_idx, row) in self.map.iter().enumerate() {
            let mut highest: i8 = -1;
            for (row_idx, &val) in row.iter().enumerate() {
                if val as i8 > highest {
                    visible.insert((col_idx, row_idx));
                    highest = val as i8;
                }
            }
            highest = -1;
            for (row_idx, &val) in row.iter().enumerate().rev() {
                if val as i8 > highest {
                    visible.insert((col_idx, row_idx));
                    highest = val as i8;
                }
            }
        }

        // visible from top, visible from bottom
        let col_size = self.map.len();
        let row_size = self.map[0].len();

        for row_idx in 0..row_size {
            let mut highest: i8 = -1;

            for col_idx in 0..col_size {
                let val = self.map[col_idx][row_idx];
                if val as i8 > highest {
                    visible.insert((col_idx, row_idx));
                    highest = val as i8;
                }
            }
            highest = -1;
            for col_idx in (0..col_size).rev() {
                let val = self.map[col_idx][row_idx];
                if val as i8 > highest {
                    visible.insert((col_idx, row_idx));
                    highest = val as i8;
                }
            }
        }

        Ok(visible.len())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let col_size = self.map.len();
        let row_size = self.map[0].len();

        let mut best_view = 0;
        for col_idx in 1..col_size - 1 {
            for row_idx in 1..row_size - 1 {
                let score = self.get_view_score(col_idx, row_idx);

                if score > best_view {
                    best_view = score;
                }
            }
        }

        Ok(best_view)
    }
}
