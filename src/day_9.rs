use std::collections::HashSet;

use crate::Solver;

#[derive(Eq, PartialEq, Hash, Debug, Clone)]
struct Coord {
    x: isize,
    y: isize,
}

impl Coord {
    fn move_towards(&mut self, towards: &Coord) {
        let delta = (towards.x - self.x, towards.y - self.y);

        let update = match delta {
            // same coord, no change
            (0, 0) => (0, 0),
            // adjacent, no change
            (-1..=1, -1..=1) => (0, 0),
            // right
            (2, 0) => (1, 0),
            // left
            (-2, 0) => (-1, 0),
            // up
            (0, 2) => (0, 1),
            // down
            (0, -2) => (0, -1),
            // diagonal right
            (-1, 2) => (-1, 1),
            (1, 2) => (1, 1),
            // diagonal left
            (-1, -2) => (-1, -1),
            (1, -2) => (1, -1),
            // diagonal up
            (2, -1) => (1, -1),
            (2, 1) => (1, 1),
            // diagonal down
            (-2, -1) => (-1, -1),
            (-2, 1) => (-1, 1),

            (-2, -2) => (-1, -1),
            (-2, 2) => (-1, 1),
            (2, 2) => (1, 1),
            (2, -2) => (1, -1),

            (x, y) => panic!("Unexpected match case! ({},{})", x, y),
        };

        self.x += update.0;
        self.y += update.1;
    }
}

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

pub struct DayNineSolver {
    instructions: Vec<(Direction, usize)>,
}

impl Solver for DayNineSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DayNineSolver {
            instructions: input
                .lines()
                .map(|line| {
                    if let Some((dir_str, count)) = line.split_once(' ') {
                        let direction = match dir_str {
                            "U" => Direction::Up,
                            "D" => Direction::Down,
                            "L" => Direction::Left,
                            "R" => Direction::Right,
                            _ => panic!("Unexpected direction"),
                        };
                        (direction, count.parse().unwrap())
                    } else {
                        panic!("Could not split line");
                    }
                })
                .collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut visited: HashSet<Coord> = HashSet::new();

        let mut head: Coord = Coord { x: 0, y: 0 };
        let mut tail = Coord { x: 0, y: 0 };
        visited.insert(tail.clone());
        for (dir, count) in self.instructions.iter() {
            for _ in 0..*count {
                match dir {
                    Direction::Up => head.y += 1,
                    Direction::Down => head.y -= 1,
                    Direction::Left => head.x -= 1,
                    Direction::Right => head.x += 1,
                }
                tail.move_towards(&head);
                visited.insert(tail.clone());
            }
        }
        Ok(visited.len())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut visited: HashSet<Coord> = HashSet::new();

        let mut rope: Vec<Coord> = vec![];
        for _ in 0..10 {
            rope.push(Coord { x: 0, y: 0 })
        }
        visited.insert(Coord { x: 0, y: 0 });
        for (dir, count) in self.instructions.iter() {
            for _ in 0..*count {
                match dir {
                    Direction::Up => rope.get_mut(0).unwrap().y += 1,
                    Direction::Down => rope.get_mut(0).unwrap().y -= 1,
                    Direction::Left => rope.get_mut(0).unwrap().x -= 1,
                    Direction::Right => rope.get_mut(0).unwrap().x += 1,
                }
                for index in 1..rope.len() {
                    let current = rope.get_mut(index - 1..=index).unwrap();
                    current[1].move_towards(&current[0].clone());
                }
                visited.insert(rope.last().unwrap().clone());
            }
        }
        Ok(visited.len())
    }
}
