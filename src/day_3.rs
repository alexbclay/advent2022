use std::collections::HashSet;

use crate::Solver;

pub struct DayThreeSolver {
    packs: Vec<(Vec<char>, Vec<char>)>,
    triples: Vec<(Vec<char>, Vec<char>, Vec<char>)>,
}

fn letter_to_number(letter: char) -> usize {
    match letter {
        'a'..='z' => letter as usize - 'a' as usize + 1,
        'A'..='Z' => letter as usize - 'A' as usize + 27,
        _ => panic!("Unexpected letter"),
    }
}

impl Solver for DayThreeSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DayThreeSolver {
            packs: input
                .lines()
                .map(|line| {
                    let (first, second) = line.split_at(line.len() / 2);
                    (first.chars().collect(), second.chars().collect())
                })
                .collect(),
            triples: input
                .lines()
                .step_by(3)
                .zip(
                    input
                        .lines()
                        .skip(1)
                        .step_by(3)
                        .zip(input.lines().skip(2).step_by(3)),
                )
                .map(|(one, (two, three))| {
                    (
                        one.chars().collect(),
                        two.chars().collect(),
                        three.chars().collect(),
                    )
                })
                .collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self
            .packs
            .iter()
            .map(|(first, second)| -> char {
                let mut first_set: HashSet<char> = HashSet::new();
                first.iter().for_each(|&item| {
                    first_set.insert(item);
                });
                for item in second {
                    if first_set.contains(item) {
                        return *item;
                    }
                }
                panic!("No overlapping characters!")
            })
            .map(letter_to_number)
            .sum())
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self
            .triples
            .iter()
            .map(|(first, second, third)| -> char {
                let mut first_set: HashSet<char> = HashSet::new();
                let mut second_set: HashSet<char> = HashSet::new();

                for &letter in first {
                    first_set.insert(letter);
                }
                for &letter in second {
                    second_set.insert(letter);
                }
                let intersection: HashSet<&char> = first_set.intersection(&second_set).collect();

                for letter in third {
                    if intersection.contains(letter) {
                        return *letter;
                    }
                }
                panic!("No overlapping characters!");
            })
            .map(letter_to_number)
            .sum())
    }
}
