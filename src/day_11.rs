use crate::Solver;

#[derive(Debug, Clone)]
enum Operand {
    Old,
    Num(usize),
}

#[derive(Debug, Clone)]
enum Operator {
    Plus,
    Multiply,
}

#[derive(Debug, Clone)]
struct Monkey {
    operation: (Operand, Operator, Operand),
    test: usize,
    true_dest: usize,
    false_dest: usize,
}

type ItemDestination = (usize, Vec<usize>);

impl Monkey {
    fn inspect(
        &self,
        items: &Vec<usize>,
        worry_reduction: usize,
        magic_number: usize,
    ) -> (ItemDestination, ItemDestination, usize) {
        let (true_items, false_items): (Vec<usize>, Vec<usize>) = items
            .iter()
            .map(|&item| {
                // increase worry level
                let lhs = match self.operation.0 {
                    Operand::Old => item,
                    Operand::Num(n) => n,
                };
                let rhs = match self.operation.2 {
                    Operand::Old => item,
                    Operand::Num(n) => n,
                };
                match self.operation.1 {
                    Operator::Plus => lhs + rhs,
                    Operator::Multiply => lhs * rhs,
                }
            })
            .map(|item| item / worry_reduction)
            .map(|item| item % magic_number)
            .partition(|item| item % self.test == 0);

        // where to send which items
        (
            (self.true_dest, true_items),
            (self.false_dest, false_items),
            items.len(),
        )
    }
}

#[derive(Clone)]
pub struct DayElevenSolver {
    items: Vec<Vec<usize>>,
    monkeys: Vec<Monkey>,
    handled_times: Vec<usize>,
    // product of all the tests (since they are all prime).  We can mod the items to make sure they don't get too big
    magic_number: usize,
}

impl DayElevenSolver {
    fn monkey_inspect(&mut self, monkey_index: usize, worry_reduction: usize) {
        let cur_monkey = &self.monkeys[monkey_index];
        let cur_items = &self.items[monkey_index];

        let res = cur_monkey.inspect(cur_items, worry_reduction, self.magic_number);
        self.items[monkey_index] = Vec::new();

        {
            let first_set = self.items.get_mut(res.0 .0).unwrap();
            res.0
                 .1
                .iter()
                .for_each(|new_item| first_set.push(*new_item))
        }

        {
            let second_set = self.items.get_mut(res.1 .0).unwrap();
            res.1
                 .1
                .iter()
                .for_each(|new_item| second_set.push(*new_item))
        }
        self.handled_times[monkey_index] += res.2;
    }
}

impl Solver for DayElevenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let (monkeys, items, handled_times, magic_number): (
            Vec<Monkey>,
            Vec<Vec<usize>>,
            Vec<usize>,
            usize,
        ) = input
            .split("\n\n")
            .map(|monkey_data| {
                // drop the id line
                let mut lines = monkey_data.lines().skip(1);

                // starting items
                let items: Vec<usize>;
                if let Some((_, items_string)) = lines.next().unwrap().split_once(": ") {
                    items = items_string
                        .split(", ")
                        .map(|num| num.parse().unwrap())
                        .collect();
                } else {
                    panic!("No items could be parsed!")
                }
                // operation
                let lhs: Operand;
                let rhs: Operand;
                let op: Operator;

                if let Some((_, op_string)) = lines.next().unwrap().split_once("new =") {
                    let parts: Vec<&str> = op_string.split_ascii_whitespace().collect();
                    if parts[0] == "old" {
                        lhs = Operand::Old
                    } else {
                        lhs = Operand::Num(parts[0].parse().unwrap())
                    }

                    if parts[1] == "*" {
                        op = Operator::Multiply;
                    } else {
                        op = Operator::Plus;
                    }

                    if parts[2] == "old" {
                        rhs = Operand::Old;
                    } else {
                        rhs = Operand::Num(parts[2].parse().unwrap())
                    }
                } else {
                    panic!("Could not parse operation")
                }

                // test
                let test: usize;
                if let Some((_, test_string)) = lines.next().unwrap().split_once("by ") {
                    test = test_string.parse().unwrap();
                } else {
                    panic!("Could not parse test")
                }

                // if true
                let true_dest: usize;
                if let Some((_, monkey_string)) = lines.next().unwrap().split_once("monkey ") {
                    true_dest = monkey_string.parse().unwrap();
                } else {
                    panic!("Could not parse true dest");
                }

                // if false
                let false_dest: usize;
                if let Some((_, monkey_string)) = lines.next().unwrap().split_once("monkey ") {
                    false_dest = monkey_string.parse().unwrap();
                } else {
                    panic!("Could not parse false dest");
                }
                (
                    Monkey {
                        operation: (lhs, op, rhs),
                        test,
                        true_dest,
                        false_dest,
                    },
                    items,
                )
            })
            .fold(
                (vec![], vec![], vec![], 1),
                |(mut monkeys, mut items, mut handled, magic_num), (cur_monkey, cur_items)| {
                    let new_num = cur_monkey.test * magic_num;
                    monkeys.push(cur_monkey);
                    items.push(cur_items);
                    handled.push(0);
                    (monkeys, items, handled, new_num)
                },
            );
        Ok(Box::new(DayElevenSolver {
            monkeys,
            items,
            handled_times,
            magic_number,
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut solution: DayElevenSolver = self.clone();
        let rounds = 20;
        for _ in 0..rounds {
            for monkey_index in 0..solution.monkeys.len() {
                solution.monkey_inspect(monkey_index, 3);
            }
        }
        let (max, next_max): (usize, usize) =
            solution
                .handled_times
                .iter()
                .fold((0, 0), |(max, next_max), &count| {
                    if count > max {
                        (count, max)
                    } else if count > next_max {
                        (max, count)
                    } else {
                        (max, next_max)
                    }
                });
        println!("{}, {}", max, next_max);
        Ok(max * next_max)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut solution: DayElevenSolver = self.clone();
        let rounds = 10000;
        for _ in 0..rounds {
            for monkey_index in 0..solution.monkeys.len() {
                solution.monkey_inspect(monkey_index, 1);
            }
        }
        let (max, next_max): (usize, usize) =
            solution
                .handled_times
                .iter()
                .fold((0, 0), |(max, next_max), &count| {
                    if count > max {
                        (count, max)
                    } else if count > next_max {
                        (max, count)
                    } else {
                        (max, next_max)
                    }
                });
        Ok(max * next_max)
    }
}
