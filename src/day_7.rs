use std::collections::HashMap;

use crate::Solver;

pub struct DaySevenSolver {
    //map files for each directory path
    files: HashMap<String, Vec<(String, usize)>>,
    // map child directories
    dirs: HashMap<String, Vec<String>>,
}

impl Solver for DaySevenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut child_files: HashMap<String, Vec<(String, usize)>> = HashMap::new();
        let mut child_dirs: HashMap<String, Vec<String>> = HashMap::new();
        let mut dir_stack: Vec<String> = vec![];
        for line in input.lines() {
            println!("{:?} - {:?}", dir_stack, line);
            // line is a command
            if line.starts_with('$') {
                let parts: Vec<&str> = line.split(' ').collect();
                // line is a 'cd'
                if parts[1] == "cd" {
                    if parts[2] == ".." {
                        dir_stack.pop();
                    } else {
                        dir_stack.push(String::from(parts[2]));
                        // make sure there's an entry for this dir in both maps
                        child_dirs.entry(dir_stack.join(":")).or_default();
                        child_files.entry(dir_stack.join(":")).or_default();
                    }
                } else {
                    // no action needed for listing
                    // println!("ls");
                }
            } else if line.starts_with("dir") {
                let parts: Vec<&str> = line.split(' ').collect();
                let name = parts[1];

                // add subdir to the current dir
                child_dirs
                    .entry(dir_stack.join(":"))
                    .or_default()
                    .push(String::from(name));
                // make sure there's an entry for both maps for the new dir
                child_dirs
                    .entry(format!("{}:{}", dir_stack.join(":"), name))
                    .or_default();
                child_files
                    .entry(format!("{}:{}", dir_stack.join(":"), name))
                    .or_default();
            } else {
                println!("--> file!");
                let parts: Vec<&str> = line.split(' ').collect();
                let size: usize = parts[0].parse().unwrap();
                let name = parts[1];
                child_files
                    .entry(dir_stack.join(":"))
                    .or_default()
                    .push((String::from(name), size));
            }
        }

        Ok(Box::new(DaySevenSolver {
            files: child_files,
            dirs: child_dirs,
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut dirs_to_check: Vec<String> = vec!["/".to_string()];
        let mut dir_sizes: HashMap<String, usize> = HashMap::new();
        while !dirs_to_check.is_empty() {
            let cur_dir = dirs_to_check.last().unwrap().to_string();
            println!("Dirs to check: {:?}", dirs_to_check);
            println!("CUR DIR: {:?}", cur_dir);
            let mut cur_size = 0;
            let mut incomplete = false;
            if let Some(sub_dirs) = self.dirs.get(&cur_dir) {
                println!("has sub dirs: {:?}", sub_dirs);
                for sub in sub_dirs {
                    if let Some(size) = dir_sizes.get(&format!("{}:{}", cur_dir, sub)) {
                        cur_size += size;
                    } else {
                        dirs_to_check.push(format!("{}:{}", cur_dir, sub));
                        incomplete = true;
                    }
                }
            }

            if !incomplete {
                if let Some(sub_files) = self.files.get(&cur_dir) {
                    println!("Adding up files! {:?}", sub_files);
                    for (_name, size) in sub_files {
                        cur_size += size;
                    }
                }

                dirs_to_check.pop();
                *dir_sizes.entry(cur_dir).or_default() += cur_size;
            }
        }
        println!("{:?}", dir_sizes);
        let mut total = 0;
        for &size in dir_sizes.values() {
            if size <= 100000 {
                total += size
            }
        }
        Ok(total)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut dirs_to_check: Vec<String> = vec!["/".to_string()];
        let mut dir_sizes: HashMap<String, usize> = HashMap::new();
        while !dirs_to_check.is_empty() {
            let cur_dir = dirs_to_check.last().unwrap().to_string();
            println!("Dirs to check: {:?}", dirs_to_check);
            println!("CUR DIR: {:?}", cur_dir);
            let mut cur_size = 0;
            let mut incomplete = false;
            if let Some(sub_dirs) = self.dirs.get(&cur_dir) {
                println!("has sub dirs: {:?}", sub_dirs);
                for sub in sub_dirs {
                    if let Some(size) = dir_sizes.get(&format!("{}:{}", cur_dir, sub)) {
                        cur_size += size;
                    } else {
                        dirs_to_check.push(format!("{}:{}", cur_dir, sub));
                        incomplete = true;
                    }
                }
            }

            if !incomplete {
                if let Some(sub_files) = self.files.get(&cur_dir) {
                    println!("Adding up files! {:?}", sub_files);
                    for (_name, size) in sub_files {
                        cur_size += size;
                    }
                }

                dirs_to_check.pop();
                *dir_sizes.entry(cur_dir).or_default() += cur_size;
            }
        }
        println!("{:?}", dir_sizes);
        let used = dir_sizes.get(&"/".to_string()).unwrap();
        let needed = 30000000 - (70000000 - used);
        Ok(*dir_sizes
            .values()
            .filter(|&size| size >= &needed)
            .min()
            .unwrap())
    }
}
