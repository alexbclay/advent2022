use crate::Solver;

#[derive(Debug)]
enum Instruction {
    Noop,
    Addx(isize),
}

pub struct DayTenSolver {
    instructions: Vec<Instruction>,
}

impl Solver for DayTenSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DayTenSolver {
            instructions: input
                .lines()
                .map(|line| {
                    if line.starts_with("addx") {
                        if let Some((_, num)) = line.split_once(' ') {
                            Instruction::Addx(num.parse().unwrap())
                        } else {
                            panic!("Could not split line!")
                        }
                    } else {
                        Instruction::Noop
                    }
                })
                .collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut cycle: isize = 0;
        let mut register: isize = 1;
        let mut target_cycle = 20;
        let mut signals: isize = 0;
        for instruct in self.instructions.iter() {
            let (cycle_bump, register_bump) = match instruct {
                Instruction::Noop => (1, 0),
                Instruction::Addx(num) => (2, *num),
            };
            if cycle + cycle_bump >= target_cycle && cycle < target_cycle {
                signals += target_cycle * register;
                target_cycle += 40;
            }
            cycle += cycle_bump;
            register += register_bump;
        }

        Ok(signals as usize)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let (pixel_array, _, _) = self.instructions.iter().fold(
            (vec![], 1, 0),
            |(mut array, register, mut scan_index), instruct| {
                let (cycle_bump, register_bump) = match instruct {
                    Instruction::Noop => (1, 0),
                    Instruction::Addx(num) => (2, *num),
                };
                for _ in 0..cycle_bump {
                    if ((register - 1)..=(register + 1)).contains(&scan_index) {
                        array.push(true);
                    } else {
                        array.push(false);
                    }
                    scan_index += 1;
                    scan_index %= 40;
                }

                (array, register + register_bump, scan_index)
            },
        );
        // print instead of Ok(num)
        for (index, pixel) in pixel_array.iter().enumerate() {
            print!("{}", if *pixel { '#' } else { '.' });
            if (index + 1) % 40 == 0 {
                println!()
            }
        }
        Ok(2)
    }
}
