use crate::Solver;

pub struct DayFourSolver {
    jobs: Vec<(usize, usize, usize, usize)>,
}

impl Solver for DayFourSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DayFourSolver {
            jobs: input
                .lines()
                .map(|line| {
                    let parts: Vec<&str> = line.split(',').collect();
                    let first_parts: Vec<usize> = parts[0]
                        .split('-')
                        .map(|num| num.parse().unwrap())
                        .collect();
                    let second_parts: Vec<usize> = parts[1]
                        .split('-')
                        .map(|num| num.parse().unwrap())
                        .collect();

                    (
                        first_parts[0],
                        first_parts[1],
                        second_parts[0],
                        second_parts[1],
                    )
                })
                .collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self
            .jobs
            .iter()
            .map(|(low_1, high_1, low_2, high_2)| {
                if (low_1 <= low_2 && high_1 >= high_2) || (low_2 <= low_1 && high_2 >= high_1) {
                    1
                } else {
                    0
                }
            })
            .sum())
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self
            .jobs
            .iter()
            .map(|(low_1, high_1, low_2, high_2)| {
                if (high_1 < low_2) || (high_2 < low_1) || (low_1 > high_2) || (low_2 > high_1) {
                    0
                } else {
                    1
                }
            })
            .sum())
    }
}
