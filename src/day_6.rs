use std::collections::HashSet;

use crate::Solver;

pub struct DaySixSolver {
    chars: Vec<char>,
}

impl Solver for DaySixSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        Ok(Box::new(DaySixSolver {
            chars: input.lines().next().unwrap().chars().collect(),
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut solution = 4;
        loop {
            // get characters
            let cur_chars = self.chars.get(solution - 4..solution).unwrap();
            let cur_set: HashSet<&char> = HashSet::from_iter(cur_chars.iter());
            if cur_set.len() == 4 {
                // no duplicate!
                break;
            }
            // there is a duplicate, so push this character and keep going
            solution += 1;
        }

        Ok(solution)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut solution = 14;
        loop {
            // get characters
            let cur_chars = self.chars.get(solution - 14..solution).unwrap();
            let cur_set: HashSet<&char> = HashSet::from_iter(cur_chars.iter());
            if cur_set.len() == 14 {
                // no duplicate!
                break;
            }
            // there is a duplicate, so push this character and keep going
            solution += 1;
        }

        Ok(solution)
    }
}
