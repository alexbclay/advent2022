pub struct DayOneSolver {
    packs: Vec<usize>,
}

impl crate::Solver for DayOneSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let packs: Vec<usize> = input.lines().fold(vec![0], |mut accumulator, element| {
            if element.is_empty() {
                accumulator.push(0);
            } else {
                let cur_val: usize = accumulator.pop().unwrap() + element.parse::<usize>().unwrap();
                accumulator.push(cur_val);
            }
            accumulator
        });

        Ok(Box::new(DayOneSolver { packs }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(*self.packs.iter().max().unwrap())
    }

    fn part_two(&self) -> Result<usize, &str> {
        let (top, three, vals) = self.packs.iter().fold((0, 0, 0), |mut best, cur| {
            if cur > &best.0 {
                best.1 = best.0;
                best.2 = best.1;
                best.0 = *cur;
            } else if cur > &best.1 {
                best.2 = best.1;
                best.1 = *cur;
            } else if cur > &best.2 {
                best.2 = *cur;
            }

            best
        });

        Ok(top + three + vals)
    }
}
