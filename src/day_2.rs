use crate::Solver;

#[derive(Debug)]
enum RPS {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug)]
enum STATE {
    Win,
    Lose,
    Draw,
}

fn rps_from_char(letter: &char) -> RPS {
    match letter {
        'A' => RPS::Rock,
        'B' => RPS::Paper,
        'C' => RPS::Scissors,
        'X' => RPS::Rock,
        'Y' => RPS::Paper,
        'Z' => RPS::Scissors,
        _ => panic!("Unexpected character: '{}'", letter),
    }
}

fn state_from_char(letter: &char) -> STATE {
    match letter {
        'X' => STATE::Lose,
        'Y' => STATE::Draw,
        'Z' => STATE::Win,
        _ => panic!("Unexpected character: '{}'", letter),
    }
}

fn value(theirs: &RPS, mine: &RPS) -> usize {
    let mut value = match mine {
        RPS::Rock => 1,
        RPS::Paper => 2,
        RPS::Scissors => 3,
    };

    value += match (theirs, mine) {
        (RPS::Rock, RPS::Rock) => 3,
        (RPS::Rock, RPS::Paper) => 6,
        (RPS::Rock, RPS::Scissors) => 0,
        (RPS::Paper, RPS::Rock) => 0,
        (RPS::Paper, RPS::Paper) => 3,
        (RPS::Paper, RPS::Scissors) => 6,
        (RPS::Scissors, RPS::Rock) => 6,
        (RPS::Scissors, RPS::Paper) => 0,
        (RPS::Scissors, RPS::Scissors) => 3,
    };

    value
}

fn value_pt_2(theirs: &RPS, goal: &STATE) -> usize {
    let mine = match (theirs, goal) {
        (RPS::Rock, STATE::Win) => RPS::Paper,
        (RPS::Rock, STATE::Lose) => RPS::Scissors,
        (RPS::Rock, STATE::Draw) => RPS::Rock,
        (RPS::Paper, STATE::Win) => RPS::Scissors,
        (RPS::Paper, STATE::Lose) => RPS::Rock,
        (RPS::Paper, STATE::Draw) => RPS::Paper,
        (RPS::Scissors, STATE::Win) => RPS::Rock,
        (RPS::Scissors, STATE::Lose) => RPS::Paper,
        (RPS::Scissors, STATE::Draw) => RPS::Scissors,
    };
    value(theirs, &mine)
}

pub struct DayTwoSolver {
    games: Vec<(char, char)>,
}

impl Solver for DayTwoSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let games: Vec<(char, char)> = input
            .lines()
            .map(|line| line.split_at(1))
            .map(|(first, second)| {
                (
                    first.chars().next().unwrap(),
                    second.trim().chars().next().unwrap(),
                )
            })
            .collect();

        Ok(Box::new(DayTwoSolver { games }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        Ok(self
            .games
            .iter()
            .map(|(first, second)| -> (RPS, RPS) { (rps_from_char(first), rps_from_char(second)) })
            .fold(0, |current, (theirs, mine)| current + value(&theirs, &mine)))
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(self
            .games
            .iter()
            .map(|(first, second)| (rps_from_char(first), state_from_char(second)))
            .fold(0, |current, (theirs, goal)| {
                current + value_pt_2(&theirs, &goal)
            }))
    }
}
