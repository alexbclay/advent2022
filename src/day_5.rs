use crate::Solver;

#[derive(Debug)]
pub struct DayFiveSolver {
    instructions: Vec<Instruction>,
    state: Vec<Vec<char>>,
}

#[derive(Debug)]
struct Instruction {
    count: usize,
    from: usize,
    to: usize,
}

impl Solver for DayFiveSolver {
    fn from_input(input: &str) -> Result<Box<Self>, String>
    where
        Self: Sized,
    {
        let mut lines_iter = input.lines();

        // extract the state until the first empty line
        let mut state: Vec<Vec<char>> = vec![
            vec![],
            vec![],
            vec![],
            vec![],
            vec![],
            vec![],
            vec![],
            vec![],
            vec![],
        ];

        loop {
            let next_line = lines_iter.next().unwrap();
            if next_line.is_empty() {
                break;
            }
            let line: Vec<char> = next_line.chars().skip(1).step_by(4).collect();
            println!("{:?}", line);
            for (index, letter) in line.into_iter().enumerate() {
                if letter == '1' {
                    // ignore the line with the labels
                    break;
                }
                if letter != ' ' {
                    state[index].push(letter);
                }
            }
        }

        // reverse each stack, so that the most recently seen line (lower in file) is at the bottom.
        // this way, we can use `push` and `pop` to move letters around.
        state.iter_mut().for_each(|vec| {
            vec.reverse();
        });

        println!("{:?}", state);

        let instructions: Vec<Instruction> = lines_iter
            .map(|line| {
                let mut chars = line.chars();
                // skip "move "
                chars.next();
                chars.next();
                chars.next();
                chars.next();
                chars.next();

                // extract all digits
                let mut count: String = String::new();
                loop {
                    let next_char = chars.next().unwrap();
                    if next_char == ' ' {
                        break;
                    }
                    count.push(next_char)
                }

                // skip "from "
                chars.next();
                chars.next();
                chars.next();
                chars.next();
                chars.next();

                // next digit is from
                let from: String = String::from(chars.next().unwrap());

                // skip " to "
                chars.next();
                chars.next();
                chars.next();
                chars.next();

                // next digit is to
                let to: String = String::from(chars.next().unwrap());

                Instruction {
                    count: count.parse().unwrap(),
                    from: from.parse().unwrap(),
                    to: to.parse().unwrap(),
                }
            })
            .collect();
        Ok(Box::new(DayFiveSolver {
            instructions,
            state,
        }))
    }

    fn part_one(&self) -> Result<usize, &str> {
        let mut state = self.state.clone();
        self.instructions.iter().for_each(|instruction| {
            for _ in 0..instruction.count {
                let cur_letter = state[instruction.from - 1].pop().unwrap();
                state[instruction.to - 1].push(cur_letter);
            }
        });

        // get final result
        let mut solution = String::from("");
        for vec in state {
            if let Some(&letter) = vec.last() {
                solution.push(letter);
            }
        }
        println!("Part 1: {}", solution);
        Ok(1)
    }

    fn part_two(&self) -> Result<usize, &str> {
        let mut state = self.state.clone();
        self.instructions.iter().for_each(|instruction| {
            let mut tmp = vec![];
            for _ in 0..instruction.count {
                let cur_letter = state[instruction.from - 1].pop().unwrap();
                tmp.push(cur_letter);
            }

            for _ in 0..instruction.count {
                let cur_letter = tmp.pop().unwrap();
                state[instruction.to - 1].push(cur_letter);
            }
        });

        // get final result
        let mut solution = String::from("");
        for vec in state {
            if let Some(&letter) = vec.last() {
                solution.push(letter);
            }
        }
        println!("Part 2: {}", solution);
        Ok(2)
    }
}
